<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Agregar Estudiante</title>
    <style>
        body {
            background-color: #2c2c2c; 
            color: #f0f0f0;
            font-family: Arial, sans-serif; 
            margin: 0;
            padding: 20px;
        }
        form {
            background-color: #333; 
            padding: 20px;
            border-radius: 10px;
            box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1);
            margin-top: 20px;
        }
        ul {
            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
            background-color: black;
            width: 100%;
        }
        
        li {
            float: left;
        }
        
        li a {
            display: block;
            color: white;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none;
        }
        
        li a:hover {
            background-color: #111;
        }

        li a.active {
            background-color: #4CAF50;
        }
        input[type="text"], input[type="number"], input[type="email"], select {
            padding: 8px;
            margin-top: 5px;
            margin-bottom: 10px;
            border-radius: 5px;
            border: 1px solid #ddd;
            background-color: #555;
            color: white;
            width: 150px;
        }
        
        input[type="submit"] {
            background-color: #4CAF50; 
            color: white;
            padding: 10px 15px;
            border: none;
            border-radius: 5px;
            cursor: pointer;
        }
        
        input[type="submit"]:hover {
            background-color: #45a049; 
        }
    </style>
</head>
<body>
    <ul>
        <li><a href="read.php">Inicio</a></li>
        <li><a class="active" href="create.php">Crear</a></li>
        <li><a href="buscarordenar.php">Buscar</a></li>
    </ul>

    <form method="post" action="" enctype="multipart/form-data">
        Nombre: <input type="text" name="nombre" required><br><br>
        Edad: <input type="number" name="edad" required><br><br>
        Correo electrónico: <input type="email" name="email" required><br><br>
        Foto de perfil: <input type="file" name="foto_perfil"  accept="image/*" required><br><br>
        <input type="submit" value="Crear">
    </form>

    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $nombre = $_POST['nombre'];
        $edad = $_POST['edad'];
        $email = $_POST['email'];
        $foto_perfil = $_FILES['foto_perfil']['name'];

        if ($foto_perfil) {
            $targetDir = __DIR__ . "/img/";
            $targetFile = $targetDir . basename($foto_perfil);
            if (!move_uploaded_file($_FILES['foto_perfil']['tmp_name'], $targetFile)) {
                die("Error al subir la imagen.");
            }
        }
    

        $conn = mysqli_connect('localhost', 'root', '', 'estudiantes');
        
        if (!$conn) {
            die("Conexión fallida: " . mysqli_connect_error());
        }

        
        $stmt = $conn->prepare("INSERT INTO datos (nombre, edad, email, foto_perfil) VALUES (?, ?, ?, ?)");
        $stmt->bind_param("siss", $nombre, $edad, $email, $foto_perfil); 

        if ($stmt->execute()) {
            header('Location: read.php');
            exit;
        } else {
            echo "Error: " . $stmt->error;
        }

        $stmt->close();
        mysqli_close($conn);
    }
    ?>
</body>
</html>
