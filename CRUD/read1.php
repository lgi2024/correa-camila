<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Inicio</title>
    <style>
        body {
            background-color: #2c2c2c;
            color: #f0f0f0;
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 20px;

        }

        ul {
            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
            background-color: #333;
            border-radius: 8px;
        }
        
        li {
            float: left;
        }
        
        li a {
            display: block;
            color: white;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none;
        }
        
        li a:hover {
            background-color: #111;
        }
        li a.active {
            background-color: #4CAF50;
        }

        table {
            width: 100%;
            border-collapse: collapse;
            margin-top: 20px;
            background-color: #3c3c3c;
            border-radius: 10px;
            overflow: hidden;
        }

        th, td {
            padding: 12px;
            text-align: left;
            border-bottom: 1px solid #444;
            color: #f0f0f0;
        }

        th {
            background-color: #555;
            color: white;
        }

        tr:hover {
            background-color: #444;
        }

        button {
            background-color: #4CAF50;
            color: white;
            border: none;
            padding: 8px 12px;
            border-radius: 4px;
            cursor: pointer;
        }

        button:hover {
            background-color: #45a049;
        }
    </style>
</head>
<body>
    <ul>
        <li><a class="active" href="read.php">Inicio</a></li>
        <li><a href="create.php">Crear</a></li>
        <li><a href="buscarordenar.php">Buscar</a></li>
    </ul>

    <?php
        $conn = mysqli_connect('localhost', 'root', '', 'estudiantes');

        if (!$conn) {
            die("Conexión fallida: " . mysqli_connect_error());
        }

        $sql = "SELECT id, nombre, edad, email, foto_perfil FROM datos";
        $result = mysqli_query($conn, $sql);

        if (mysqli_num_rows($result) > 0) {
            echo "<div style='display: flex; justify-content: center;'>";
            echo "<table><tr><th>ID</th><th>Nombre</th><th>Edad</th><th>Correo Electrónico</th><th>Foto de Perfil</th><th>Acciones</th></tr>";
            while ($row = mysqli_fetch_assoc($result)) {
                echo "<tr>";
                echo "<td>" . $row["id"]. "</td>";
                echo "<td>" . $row["nombre"]. "</td>";
                echo "<td>" . $row["edad"]. "</td>";
                echo "<td>" . $row["email"]. "</td>";
                echo "<td><img src='img/" . $row["foto_perfil"] . "' width='50' height='50'></td>";
                
                echo "<td>
                <form style='display:inline;' action='update.php' method='get'>
                    <input type='hidden' name='id' value='" . $row['id'] . "'>
                    <button type='submit'>Editar</button>
                </form>
                <form style='display:inline;' action='delete.php' method='POST'>
                    <input type='hidden' name='id' value='" . $row['id'] . "'>
                    <button type='submit'>Eliminar</button>
                </form>
                </td>";
                echo "</tr>";
            }
            echo "</table>";
            echo "</div>";
        } else {
            echo "<div style='text-align: center;'>0 resultados</div>";
        }

        mysqli_close($conn);
    ?>
</body>
</html>
