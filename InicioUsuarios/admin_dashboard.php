<?php
session_start();

if (!isset($_SESSION['user_id']) || $_SESSION['rol'] != 'administrador') {
    header("Location: login.php");
    exit();
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Panel de Administración</title>
    <style>
        body {
            background-color: #2c2c2c;
            color: #f0f0f0;
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 20px;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
        }

        .admin-container {
            background-color: #3c3c3c;
            padding: 20px;
            border-radius: 10px;
            box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.5);
            text-align: center;
            width: 300px;
        }

        h1 {
            color: #4CAF50;
            margin-bottom: 20px;
        }

        a {
            display: block;
            color: white;
            text-decoration: none;
            margin: 10px 0;
            padding: 10px;
            background-color: #555;
            border-radius: 5px;
            transition: background-color 0.3s;
        }

        a:hover {
            background-color: #4CAF50;
        }
    </style>
</head>
<body>
    <div class="admin-container">
        <?php
        echo "<h1>Bienvenido Administrador, " . $_SESSION['username'] . "</h1>";
        ?>
        <a href="read.php">Gestionar Alumnos</a>
        <a href="logout.php">Cerrar sesión</a>
    </div>
</body>
</html>

