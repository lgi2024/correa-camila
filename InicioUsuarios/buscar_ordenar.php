<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Buscar Estudiante</title>
    <style>
        body {
            background-color: #2c2c2c; 
            color: #f0f0f0;
            font-family: Arial, sans-serif; 
            margin: 0;
            padding: 20px;
        }
        form, .resultados {
            background-color: #3c3c3c;
            color: #f0f0f0;
            padding: 20px;
            border-radius: 10px;
            box-shadow: 0px 0px 15px rgba(0, 0, 0, 0.2);
            margin-top: 20px;
        }
        ul {
            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
            background-color: #333;
            border-radius: 8px;
        }
        li {
            float: left;
        }
        li a {
            display: block;
            color: white;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none;
        }
        li a:hover {
            background-color: #111;
        }
        li a.active {
            background-color: #4CAF50; 
        }
        input[type="text"], select {
            padding: 8px;
            margin-top: 5px;
            margin-bottom: 10px;
            border-radius: 5px;
            border: 1px solid #ddd;
            background-color: #555;
            color: white;
            width: 150px; 
        }
        input[type="submit"] {
            background-color: #4CAF50;
            color: white;
            padding: 10px 20px;
            border: none;
            border-radius: 5px;
            cursor: pointer;
        }
        input[type="submit"]:hover {
            background-color: #45a049;
        }
    </style>
</head>
<body>
    <ul>
        <li><a class="active" href="buscarordenar.php">Buscar</a></li>
        <li><a href="logout.php">Cerrar sesion</a></li>
        
    </ul>

    <form method="post" action="">
        <h3>Buscar y Ordenar Estudiantes</h3>
        Nombre: <input type="text" name="buscar_nombre"><br><br>
        Edad: <input type="text" name="buscar_edad"><br><br>
        Email: <input type="text" name="buscar_email"><br><br>

        <label for="ordenar_por">Ordenar por:</label>
        <select name="ordenar_por" id="ordenar_por">
            <option value="nombre">Nombre</option>
            <option value="edad">Edad</option>
        </select><br><br>

        <label for="orden">Orden:</label>
        <select name="orden" id="orden">
            <option value="ASC">Ascendente</option>
            <option value="DESC">Descendente</option>
        </select><br><br>

        <input type="submit" name="buscar" value="Buscar">
    </form>

    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['buscar'])) {
        $buscar_nombre = $_POST['buscar_nombre'];
        $buscar_edad = $_POST['buscar_edad'];
        $buscar_email = $_POST['buscar_email'];
        $ordenar_por = $_POST['ordenar_por'];
        $orden = $_POST['orden'];

        $conn = mysqli_connect('localhost', 'root', '', 'estudiantes');
        
        if (!$conn) {
            die("Conexión fallida: " . mysqli_connect_error());
        }

        $sql = "SELECT * FROM datos WHERE 1=1";

        if (!empty($buscar_nombre)) {
            $sql .= " AND nombre LIKE '%$buscar_nombre%'";
        }
        if (!empty($buscar_edad)) {
            $sql .= " AND edad LIKE '%$buscar_edad%'";
        }
        if (!empty($buscar_email)) {
            $sql .= " AND email LIKE '%$buscar_email%'";
        }

        if (!empty($ordenar_por) && !empty($orden)) {
            $sql .= " ORDER BY $ordenar_por $orden";
        }

        $result = mysqli_query($conn, $sql);

        echo '<div class="resultados">';
        if (mysqli_num_rows($result) > 0) {
            while($row = mysqli_fetch_assoc($result)) {
                echo "Nombre: " . $row["nombre"]. " - Edad: " . $row["edad"]. " - Email: " . $row["email"]. "<br>";
            }
        } else {
            echo "No se encontraron resultados.";
        }
        echo '</div>';

        mysqli_close($conn);
    }
    ?>
</body>
</html>