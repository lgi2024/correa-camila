<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $id = $_POST['id'];
    $nombre = $_POST['nombre'];
    $edad = $_POST['edad'];
    $email = $_POST['email'];

    $conn = mysqli_connect('localhost', 'root', '', 'estudiantes');

    if (!$conn) {
        die("Conexión fallida: " . mysqli_connect_error());
    }

    
    if (!empty($_FILES['foto_perfil']['name'])) {
        $foto_perfil = $_FILES['foto_perfil']['name'];
        $targetDir = __DIR__ . "/img/";
        $targetFile = $targetDir . basename($foto_perfil);

        if (!move_uploaded_file($_FILES['foto_perfil']['tmp_name'], $targetFile)) {
            die("Error al subir la imagen.");
        }

        
        $sql = "UPDATE datos SET nombre='$nombre', edad=$edad, email='$email', foto_perfil='$foto_perfil' WHERE id=$id";
    } else {
        
        $sql = "UPDATE datos SET nombre='$nombre', edad=$edad, email='$email' WHERE id=$id";
    }

    if (mysqli_query($conn, $sql)) {
        echo "Estudiante actualizado con éxito";
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
    }

    mysqli_close($conn);
} else {
    $id = $_GET['id'];

    $conn = mysqli_connect('localhost', 'root', '', 'estudiantes');

    if (!$conn) {
        die("Conexión fallida: " . mysqli_connect_error());
    }

    $sql = "SELECT id, nombre, edad, email, foto_perfil FROM datos WHERE id=$id";
    $result = mysqli_query($conn, $sql);

    if (mysqli_num_rows($result) > 0) {
        $row = mysqli_fetch_assoc($result);
        $nombre = $row['nombre'];
        $edad = $row['edad'];
        $email = $row['email'];
        $foto_perfil = $row['foto_perfil'];
    } else {
        echo "No se encontró el estudiante con ID: $id";
        exit();
    }

    mysqli_close($conn);
}
?>

<form method="post" action="" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $id; ?>">
    Nombre: <input type="text" name="nombre" value="<?php echo $nombre; ?>" required><br>
    Edad: <input type="number" name="edad" value="<?php echo $edad; ?>" required><br>
    Correo electrónico: <input type="email" name="email" value="<?php echo $email; ?>" required><br>
    Foto de perfil: <input type="file" name="foto_perfil" accept="image/*"><br>
    <input type="submit" value="Actualizar">
</form>
